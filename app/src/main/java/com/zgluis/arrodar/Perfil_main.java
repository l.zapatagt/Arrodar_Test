package com.zgluis.arrodar;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zgluis.arrodar.model.Url;
import com.zgluis.arrodar.util.Barrasnack;
import com.zgluis.arrodar.util.Progress;
import com.zgluis.arrodar.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Perfil_main extends AppCompatActivity implements Perfil_actionbar.OnFragmentInteractionListener {

    private String encoded_string, image_name;
    private Bitmap bitmap;
    private File file;
    private Uri file_uri;
    private ImageButton ib_perfil;
    SharedPreferences session;
    private EditText et_name,et_lastname,et_address,et_telf;
    private Switch sw_car;
    private AlertDialog.Builder builder;
    private String TAG = Utils.getTag(this);
    private ProgressDialog pDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_ACTION_BAR);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.perfil_main);
        session = getSharedPreferences("session", Context.MODE_PRIVATE);
        Log.i("Arrodar_Perfil", "OnCreate Perfil");
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        Perfil_actionbar fragment = new Perfil_actionbar();
        transaction.add(R.id.actionbar, fragment);
        transaction.commit();

        et_name = (EditText) findViewById(R.id.et_name);
        et_lastname = (EditText) findViewById(R.id.et_lastname);
        et_address = (EditText) findViewById(R.id.et_address);
        et_telf = (EditText) findViewById(R.id.et_telf);
        sw_car = (Switch) findViewById(R.id.sw_car);
        ib_perfil = (ImageButton) findViewById(R.id.ib_perfil);
        pDialog = new ProgressDialog(this);
        setImgPerfil(ib_perfil, session, TAG);
        fillForm();

        builder = new AlertDialog.Builder(this);
        builder.setTitle("Seleccionar Imagen");
        builder.setMessage("Opciones");

        builder.setPositiveButton("Camara", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                getFileUri();
                i.putExtra(MediaStore.EXTRA_OUTPUT, file_uri);
                Log.i(TAG,"Launching camara");
                startActivityForResult(i, 10);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Local", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing
                getLocalImage();
                dialog.dismiss();
            }
        });


        ib_perfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }//fin oncreate


    private void fillForm() {
        Log.i(TAG,"fillform");
        et_name.setText(session.getString("nombre", ""));
        et_lastname.setText(session.getString("apellido", ""));
        et_address.setText(session.getString("direccion", ""));
        et_telf.setText(session.getString("telefono", ""));
        if(session.getString("id_vehiculo", "").equals("0")){
            sw_car.setChecked(false);
        }else{
            sw_car.setChecked(true);
        }

    }

    public void lanzarHome(){
        Intent intent = new Intent(getApplicationContext(), Home_main.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void getFileUri() {
        image_name = session.getString("id_user", "")+".jpg";
        file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                + File.separator + image_name
        );
        file_uri = Uri.fromFile(file);
        System.out.println("Arrodar_Perfil-GetFilesUri:"+file_uri);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 10 && resultCode == RESULT_OK) {
            Log.i(TAG,"onActivityResult:10");
            new Encode_image().execute();
        }
        if (requestCode == 222 && resultCode == Activity.RESULT_OK) {

//Se trabaja en lograr convertir la imagen obtenida desde la galeria a bitmap, para trabajarla

            Uri selectedImageUri = data.getData();
            file = new File(String.valueOf(selectedImageUri));
            file_uri = Uri.fromFile(file);
            System.out.println("arrodar_perfil_UriPath=" + selectedImageUri.getPath());
            bitmap = BitmapFactory.decodeFile(selectedImageUri.getPath());
            Bitmap resized = Bitmap.createScaledBitmap(bitmap, 200, 300, true);
            Bitmap img = cropToSquare(resized);
            storeImage(img);
        }
    }

    private void getLocalImage(){
        Log.i(TAG,"getLocalImage");
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, 222);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    private class Encode_image extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            System.out.println("Arrodar_Perfil_doinbackground");
            System.out.println("arrodar_perfil_UriPath=" + file_uri.getPath());
            bitmap = BitmapFactory.decodeFile(file_uri.getPath());
            Bitmap resized = Bitmap.createScaledBitmap(bitmap, 200, 300, true);
            Bitmap img = cropToSquare(resized);
            storeImage(img);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            img.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] array = stream.toByteArray();
            encoded_string = Base64.encodeToString(array, 0);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.i(TAG, "onPostExecute");
            Progress.showProgressDialog(true, pDialog, "Cargando");

            subirImg();
        }
    }

    private void subirImg() {
        Log.i("arrodar_perfil", "MakeRequest");

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest request = new StringRequest(Request.Method.POST, Url.Subir_Img,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Progress.showProgressDialog(false, pDialog, "");

                        Log.i("arrodar_perfil", "Respuesta = "+response);

                        setImgPerfil(ib_perfil, session, TAG);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, "Error="+error);
                Barrasnack.show("Intentalo de nuevo",findViewById(android.R.id.content));
                Progress.showProgressDialog(false, pDialog, "");

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> map = new HashMap<>();
                map.put("encoded_string",encoded_string);
                map.put("image_name", image_name);
                map.put("id_user", session.getString("id_user", ""));
                System.out.println("******Java.Map: "+map+"******");
                return map;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }

    public void goSave(){
        if(TextUtils.isEmpty(Utils.getValor(et_name))|| TextUtils.isEmpty(Utils.getValor(et_lastname))||TextUtils.isEmpty(Utils.getValor(et_address))||TextUtils.isEmpty(Utils.getValor(et_telf)))
        {
            Barrasnack.show("Debe llenar todos los campos",findViewById(android.R.id.content));
        }
        else {
            Progress.showProgressDialog(true, pDialog, "Cargando");
            actualizarUser();
        }
    }

    private void actualizarUser() {

        Log.i(TAG, Url.Actualizar_User);
        JSONObject valores = new JSONObject();
        try {
            valores.put("id_user", session.getString("id_user", ""));
            valores.put("nombre",Utils.getValor(et_name));
            valores.put("apellido",Utils.getValor(et_lastname));
            valores.put("direccion",Utils.getValor(et_address));
            valores.put("telefono",Utils.getValor(et_telf));
            valores.put("id_vehiculo",Utils.getSwitch(sw_car));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println(TAG+"Valores:"+valores);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, Url.Actualizar_User, valores, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Progress.showProgressDialog(false, pDialog, "");

                        Log.i(TAG, "Respuesta = " + response);
                        Barrasnack.show("Actualizacion exitosa", findViewById(android.R.id.content));

                        setSession();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Barrasnack.show("Intentalo de nuevo",findViewById(android.R.id.content));

                        Log.i(TAG, "Error=" + error);

                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);


        VSingleton.getInstance(this).addToRequestQueue(jsObjRequest);
    }

    private void setSession() {

            SharedPreferences.Editor meditor;
            meditor = session.edit();
            meditor.putString("nombre", Utils.getValor(et_name));
            meditor.putString("apellido",Utils.getValor(et_lastname));
            meditor.putString("direccion", Utils.getValor(et_address));
            meditor.putString("telefono",Utils.getValor(et_telf));
            meditor.putString("id_vehiculo", Utils.getSwitch(sw_car));
            meditor.apply();
            lanzarHome();
    }

    public Bitmap cropToSquare(Bitmap bitmap){
        int width  = bitmap.getWidth();
        int height = bitmap.getHeight();
        int newWidth = (height > width) ? width : height;
        int newHeight = (height > width)? height - ( height - width) : height;
        int cropW = (width - height) / 2;
        cropW = (cropW < 0)? 0: cropW;
        int cropH = (height - width) / 2;
        cropH = (cropH < 0)? 0: cropH;
        return Bitmap.createBitmap(bitmap, cropW, cropH, newWidth, newHeight);
    }

    private  File getOutputMediaFile(){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        Log.i(TAG,"GetOutputMediaFile");
        Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        File mediaStorageDir;
        if(isSDPresent){
            mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                    + "/Android/data/"
                    + getApplicationContext().getPackageName()
                    + "/Files");
        }else{
            mediaStorageDir = new File(getFilesDir().getPath());
        }

        System.out.println("arrodar_perfil_ruta2:"+mediaStorageDir);

        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String aux = session.getString("id_user", "");
        String mImageName=aux +".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        System.out.println("arrodar_perfil_ruta2:"+mediaFile);

        return mediaFile;
    }

    private void storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d("arrodar_perfil",
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("arrodar_perfil", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("arrodar_perfil", "Error accessing file: " + e.getMessage());
        }
    }

    private void setImgPerfil(ImageButton ib, SharedPreferences session, String TAG) {
        Log.i(TAG,"SetImgPerfil");
        Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        String ruta;
        String aux = session.getString("id_user", "");
        if(isSDPresent){
            ruta = Environment.getExternalStorageDirectory()
                    + "/Android/data/"
                    + getApplicationContext().getPackageName()
                    + "/Files/" + aux + ".jpg";
        }else{
            ruta = getFilesDir().getPath() + aux + ".jpg";
        }

        File imgFile = new  File(ruta);
        System.out.println("arrodar_perfil_imgfile:" + imgFile);
        System.out.println("arrodar_perfil_Ruta:" + ruta);

        if(imgFile.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            ib.setImageBitmap(myBitmap);

        }else{
            ib.setImageResource(R.drawable.no_img);
        }

    }
}
