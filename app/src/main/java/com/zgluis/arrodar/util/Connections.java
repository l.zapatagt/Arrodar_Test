package com.zgluis.arrodar.util;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.zgluis.arrodar.app.AppController;
import com.zgluis.arrodar.model.Url;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class Connections extends AppCompatActivity {

    public static void consultarZonas(final ArrayList<String> zonas, final ArrayAdapter<String> adaptador, final String TAG, final ProgressDialog pDialog) {

        zonas.clear();
        adaptador.notifyDataSetChanged();
        Log.i(TAG, Url.Obtener_Zonas);
        JsonArrayRequest zonasReq = new JsonArrayRequest(
                Url.Obtener_Zonas,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        Progress.showProgressDialog(false, pDialog, "");
                        int x = response.length();
                        // Parsing json
                        for (int i = 0; i < x; i++) {
                            try {

                                JSONObject obj = response.getJSONObject(i);

                                // adding zonas al array
                                zonas.add(obj.getString("nombre"));


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        adaptador.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Progress.showProgressDialog(false, pDialog, "");


            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(zonasReq);//


    }

    public static void estatusImgUser(final String id_user, boolean state, final String TAG,  final ProgressDialog pDialog) {

        Log.i(TAG,Url.estatusImgUser+"?id_user="+id_user+"?state"+state);
        JsonArrayRequest imgReq = new JsonArrayRequest(
                Url.estatusImgUser+"?id_user="+id_user+"?state"+state,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        Progress.showProgressDialog(false, pDialog, "");
                        int x = response.length();
                        // Parsing json
                        for (int i = 0; i < x; i++) {
                            try {

                                JSONObject obj = response.getJSONObject(i);
                                obj.getString("estado");
                                System.out.println(TAG+": Response:"+response);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Progress.showProgressDialog(false, pDialog, "");
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(imgReq);//
    }

}
