package com.zgluis.arrodar;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.zgluis.arrodar.join.Join_dos;
import com.zgluis.arrodar.model.Url;
import com.zgluis.arrodar.util.Barrasnack;
import com.zgluis.arrodar.util.Encryption;
import com.zgluis.arrodar.util.Progress;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_CONTACTS;

public class Login_main extends AppCompatActivity implements LoaderCallbacks<Cursor> {



    JSONObject valores = new JSONObject();


    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    Encryption encryption;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();

        encryption = Encryption.getLowIteration("Key", "Salt", new byte[16]);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            Progress.showProgressLayout(true, mLoginFormView, mProgressView);
            mAuthTask = new UserLoginTask(Url.Login, email, encryption.encryptOrNull(password));
            mAuthTask.execute();
        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 6;
    }

    /**
     * Shows the progress UI and hides the login form.
     */


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(Login_main.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };
        int ADDRESS = 0;
        //int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<String,Void,UserLoginTask.MyResult> {

        private final String mUrl;
        private final String mEmail;
        private final String mPassword;
        SharedPreferences session;


        UserLoginTask(String url, String email, String password) {
            mUrl = url;
            mEmail = email;
            mPassword = password;
        }


        @Override
        protected MyResult doInBackground(String... params) {

            try {
                // Simulate network access.
                Thread.sleep(2000);


                HttpURLConnection urlConn;
                URL miurl = new URL(mUrl);
                urlConn = (HttpURLConnection) miurl.openConnection();
                urlConn.setDoInput(true);
                urlConn.setDoOutput(true);
                urlConn.setUseCaches(false);
                urlConn.setRequestProperty("Content-Type", "application/json");
                urlConn.setRequestProperty("Accept", "application/json");
                urlConn.connect();
                //Creo el Objeto JSON
                valores.put("email", mEmail);
                valores.put("password",mPassword);
                // Envio los parámetros post
                OutputStream os = urlConn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(valores.toString());
                writer.flush();
                writer.close();

                int respuesta = urlConn.getResponseCode();


                StringBuilder result = new StringBuilder();

                if (respuesta == HttpURLConnection.HTTP_OK) {

                    String line;
                    BufferedReader br=new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
                    while ((line=br.readLine()) != null) {
                        result.append(line);
                    }
                    JSONObject respuestaJSON = new JSONObject(result.toString());

                    String estado = respuestaJSON.getString("estado");

                    if (estado.equals("0")) {      // hay un user que mostrar
                        return null;
                    } else {
                        JSONObject contenido = respuestaJSON.getJSONObject("contenido");
                        System.out.println("Arrodar_Login: Llego un Json =" + contenido);
                        return new MyResult(contenido,estado);
                    }

                }

            } catch (InterruptedException e) {
                return null;
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

        return null;
        }

        final class MyResult{
            private final JSONObject first;
            private final String second;

            public MyResult(JSONObject first, String second) {
                this.first = first;
                this.second = second;
                Log.i("arrodar_login", "Ejecutado asignacion" );

            }

            public JSONObject getFirst() {
                Log.i("arrodar_login", "Ejecutado getFirst" );
                return first;
            }

            public String getSecond() {
                return second;
            }
        }

        @Override
        protected void onPostExecute(final MyResult contenido) {
            mAuthTask = null;
            Progress.showProgressLayout(false, mLoginFormView, mProgressView);
            String aux;
            try{
                aux = contenido.getSecond();
                Log.i("arrodar_login", "onPost return=" + aux);

                switch(aux){
                    case "1" ://match
                        JSONObject data = contenido.getFirst();
                        SharedPreferences.Editor meditor;
                        session = getSharedPreferences("session", Context.MODE_PRIVATE);
                        Log.d("arrodar_login", "Estoy en insertar");
                        meditor = session.edit();
                        meditor.putString("email", mEmail);

                        try {
                            meditor.putString("id_user", data.getString("id_user"));
                            meditor.putString("ci", data.getString("ci"));
                            meditor.putString("nombre", data.getString("nombre"));
                            meditor.putString("apellido", data.getString("apellido"));
                            meditor.putString("direccion", data.getString("direccion"));
                            meditor.putString("zona", data.getString("zona"));
                            meditor.putString("telefono", data.getString("telefono"));
                            meditor.putString("id_vehiculo", data.getString("id_vehiculo"));
                            meditor.putString("reputacion", data.getString("reputacion"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        meditor.apply();

                        Intent intent = new Intent(Login_main.this, Home_main.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;

                    case "2" ://Nuevo usuario
                        intent = new Intent(getApplicationContext(), Join_dos.class);
                        intent.putExtra("valores", valores.toString());
                        startActivity(intent);
                        break;

                    case "3" ://Usuario y contraseña no coinciden
                        mPasswordView.setError(getString(R.string.error_incorrect_password));
                        mPasswordView.requestFocus();
                        break;

                    case "null" ://Error de conexion
                        Log.i("Arrodar_Login","Fallo de conexion");
                        break;

                    default :
                        Log.i("Arrodar_Login","Respuesta no encontrada");
                        break;
                }
            }catch(NullPointerException e){
                Barrasnack.show("Ocurrio un error",findViewById(android.R.id.content));
            }


        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            Progress.showProgressLayout(false, mLoginFormView, mProgressView);
        }
    }
}

