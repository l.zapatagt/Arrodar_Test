package com.zgluis.arrodar;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.zgluis.arrodar.adapter.Viajes_adapter;
import com.zgluis.arrodar.app.AppController;
import com.zgluis.arrodar.model.Url;
import com.zgluis.arrodar.model.Viaje;
import com.zgluis.arrodar.util.Barrasnack;
import com.zgluis.arrodar.util.Progress;
import com.zgluis.arrodar.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Home_main extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    SharedPreferences session;
    AlertDialog.Builder builder;

    // Log tag
    private static final String TAG = "Arrodar_HomeMain";
    private ProgressDialog pDialog;
    private List<Viaje> viajeList = new ArrayList<>();
    private Viajes_adapter adapter;
    private String id_viaje;
    private TextView tv_empty,tv_home_email,tv_home_fname;

    AlertDialog alertCS;
    Snackbar snack;
    ImageButton ib_perfil;
    NavigationView navigationView;
            View headerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_main);

        //Declaramos los elementos del encabrzado del menu lateral
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        headerLayout = navigationView.inflateHeaderView(R.layout.nav_header_home_main);
        tv_home_fname = (TextView) headerLayout.findViewById(R.id.tv_home_fname);
        tv_home_email = (TextView) headerLayout.findViewById(R.id.tv_home_email);
        ib_perfil = (ImageButton) headerLayout.findViewById(R.id.ib_perfil);
        ib_perfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "Going to Perfil_main");
                startActivity(new Intent(getApplicationContext(), Perfil_main.class));
            }
        });

        //Le damos el contexo a session
        session = getSharedPreferences("session", Context.MODE_PRIVATE);



        //Chequeamos si hay una sesion iniciada
        if(Utils.CheckSession(this))
        {fillHeader();}

        //Barra de cabecera
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Creando menu lateral
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //Creando Alert y perzonalidandolo
        builder = new AlertDialog.Builder(this);
        builder.setTitle("Cerrar Sesion");
        builder.setMessage("¿Esta seguro de que quiere cerrar sesion?");
        builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Utils.cerrarSesion(getApplicationContext());
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertCS = builder.create();

        builder.setTitle("Reservar Viaje");
        builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Progress.showProgressDialog(true, pDialog, "Enviando");
                reservarViaje(id_viaje, session.getString("id_user", ""));
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertRV = builder.create();

        //tv que se muestra en caso de error
        tv_empty = (TextView) findViewById(R.id.tv_empty);

        //Listview con el contenido
        ListView listView;
        listView = (ListView) findViewById(R.id.list);
        adapter = new Viajes_adapter(this, viajeList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Viaje viaje = (Viaje) parent.getAdapter().getItem(position);
                id_viaje = viaje.getId_viaje();

                alertRV.setMessage("¿Estas seguro que quieres viajar con " + viaje.getNombre() + " " + viaje.getApellido() + "? Viaje: #" + viaje.getId_viaje());
                alertRV.show();

            }
        });


        //Creando Snackbar, barra que se despliega desde abajo
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Integer.parseInt( session.getString("id_vehiculo", "")  ) < 1){
                    snack = Snackbar.make(view, "Debe cargar los datos de su vehiculo", Snackbar.LENGTH_LONG);
                    view = snack.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.WHITE);
                    snack.show();
                }
                else{
                    startActivity(new Intent(getApplicationContext(), CrearV_main.class));
                }}

            });


        //Creando Dialogo de progreso de carga
        pDialog = new ProgressDialog(this);
        Progress.showProgressDialog(true, pDialog, "Cargando...");


        //Eniando Consulta
        Log.i(TAG, Url.Obtener_Viajes);
        consultarServicio();

    }


    private void consultarServicio() {
        viajeList.clear();
        adapter.notifyDataSetChanged();
        final String id_user = session.getString("id_user", "");
        Log.i(TAG,Url.Obtener_Viajes+"?id_user="+id_user);
        JsonArrayRequest movieReq = new JsonArrayRequest(
                Url.Obtener_Viajes+"?id_user="+id_user,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        Progress.showProgressDialog(false, pDialog, "");
                        int x = response.length();
                        // Parsing json
                        for (int i = 0; i < x; i++) {
                            try {

                                JSONObject obj = response.getJSONObject(i);
                                Viaje viaje = new Viaje();
                                viaje.setId_viaje(obj.getString("id_viaje"));
                                viaje.setNombre(obj.getString("nombre"));
                                viaje.setApellido(obj.getString("apellido"));
                                String ThumbUrl = Url.index+"images/"+obj.getString("img_url");
                                viaje.setThumbnailUrl(ThumbUrl);
                                viaje.setSalida(obj.getString("id_salida"));
                                viaje.setLlegada(obj.getString("id_llegada"));
                                viaje.setFecha(obj.getString("fecha"));
                                viaje.setHora(obj.getString("hora"));
                                viaje.setReputacion(obj.getString("reputacion"));
                                viaje.setEstatus(obj.getString("estatus"));

                                Log.i(TAG, ThumbUrl);

                                // adding viaje to movies array
                                viajeList.add(viaje);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Progress.showProgressDialog(false, pDialog, "");

                Log.e(TAG, "ErrorVolley:: " + error.getMessage());
                if(error.getMessage()== null ) {
                    Barrasnack.show("Intentalo de nuevo", findViewById(android.R.id.content));
                    tv_empty.setText("");
                }
                tv_empty.setText(getApplication().getString(R.string.empty));

            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(movieReq);//

    }

    private void reservarViaje(final String id_viaje, final String id_user) {
        Log.i(TAG, Url.Reservar_Viaje);
        JSONObject obj = new JSONObject();
        try {
            obj.put("id_viaje", id_viaje);
            obj.put("id_user", id_user);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, Url.Reservar_Viaje, obj, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Progress.showProgressDialog(false, pDialog, "");
                        Log.i(TAG, "Respuesta=" + response);
                        Barrasnack.show("Viaje Reservado", findViewById(android.R.id.content));
                        consultarServicio();

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Progress.showProgressDialog(false, pDialog, "");
                        Barrasnack.show("Intenta de nuevo", findViewById(android.R.id.content));
                        System.out.println("El error:" + error);
                        Log.i(TAG, "Error=" + error);
                    }
                });

        VSingleton.getInstance(this).addToRequestQueue(jsObjRequest);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Progress.showProgressDialog(false, pDialog, "");

    }

    private void fillHeader(){


        tv_home_fname.setText(getApplication().getString(R.string.fname, session.getString("nombre", ""),session.getString("apellido", "")));
        tv_home_email.setText(session.getString("email", ""));

    setImgPerfil(ib_perfil, session, TAG);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            alertCS.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_perfil) {
            Log.i("arrodar_home","Going to Perfil_main");
            startActivity(new Intent(this, Perfil_main.class));

        } else if (id == R.id.nav_viajes) {
            Log.i("arrodar_home","Going to MisViajes_main");
            startActivity(new Intent(getApplicationContext(), MisViajes_main.class));

        } else if (id == R.id.nav_car) {
            Log.i("arrodar_home","Going to Vehiculo_main");
            startActivity(new Intent(getApplicationContext(), Vehiculo_main.class));
       /* } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_help) {

        } else if (id == R.id.nav_info) {*/

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void btnConsultar(View v){
        Progress.showProgressDialog(true, pDialog, "Cargando");
        tv_empty.setText("");
        consultarServicio();
    }

    private void setImgPerfil(ImageButton ib, SharedPreferences session, String TAG) {
        Log.i(TAG,"SetImgPerfil");
        Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        String ruta;
        String aux = session.getString("id_user", "");
        if(isSDPresent){
            ruta = Environment.getExternalStorageDirectory()
                    + "/Android/data/"
                    + getApplicationContext().getPackageName()
                    + "/Files/" + aux + ".jpg";
        }else{
            ruta = getFilesDir().getPath() + aux + ".jpg";
        }

        File imgFile = new  File(ruta);
        System.out.println(TAG + " Ruta img:" + ruta);

        if(imgFile.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            ib.setImageBitmap(myBitmap);

        }else{
            ib.setImageResource(R.drawable.no_img);
        }

    }

    @Override
    protected void onRestart() {
        Log.i("TAG","onRestart");
        if(Utils.CheckSession(this))
        {fillHeader();}
        super.onRestart();
    }
}
