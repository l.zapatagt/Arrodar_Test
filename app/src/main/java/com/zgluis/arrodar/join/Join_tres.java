package com.zgluis.arrodar.join;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.zgluis.arrodar.Home_main;
import com.zgluis.arrodar.R;
import com.zgluis.arrodar.VSingleton;
import com.zgluis.arrodar.model.Url;
import com.zgluis.arrodar.util.Barrasnack;
import com.zgluis.arrodar.util.Connections;
import com.zgluis.arrodar.util.Progress;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class Join_tres extends AppCompatActivity {

    ArrayList<String> zonas= new ArrayList<>();

    JSONObject valores;
    private View mProgressView;
    private View mJoinFormView;
    private String TAG = "Join_zonas";
    ArrayAdapter<String> adaptador;
    String zona;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.join3);

        mJoinFormView = findViewById(R.id.join_form);
        mProgressView = findViewById(R.id.join_progress);

        Bundle b = getIntent().getExtras();

        if(b!=null){
            String aux = b.getString("valores");
            try {
                valores = new JSONObject(aux);
                Log.d("arrodar_J", "Valores recibidos en join3" + valores.toString());
            } catch (Throwable t) {
                Log.e("arrodar_J", "Could not parse to json Error:"+t);
            }
        }

        ProgressDialog pDialog = new ProgressDialog(this);
        Progress.showProgressDialog(true, pDialog, "Cargando...");

        ListView listview_red= (ListView) findViewById(R.id.lv_zona);
        adaptador = new ArrayAdapter<>(this, android.R.layout.simple_list_item_single_choice, zonas);
        listview_red.setAdapter(adaptador);
        listview_red.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                zona = zonas.get(position);
            }
        });

        Connections.consultarZonas(zonas, adaptador, TAG, pDialog);

    }


    public void join3_next(View view){

        if(zona == null) {
            Barrasnack.show("Debe seleccionar una opcion", findViewById(android.R.id.content));

        }
        else {
            agregar();
            Log.d(TAG, "Valores recolectados" + valores.toString());
            Progress.showProgressLayout(true, mJoinFormView, mProgressView);
            insertarUser();
                    }

    }



    public void agregar(){
        try {
            valores.put("zona", zona);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }




    private void insertarUser() {
        Log.i(TAG, Url.Insertar_User);
        System.out.println(valores);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, Url.Insertar_User, valores, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Progress.showProgressLayout(false, mJoinFormView, mProgressView);
                        Log.i(TAG, "Respuesta=" + response);

                        SharedPreferences.Editor meditor;
                        SharedPreferences session = getSharedPreferences("session", Context.MODE_PRIVATE);
                        Log.d(TAG, "Exito en registro");
                        meditor = session.edit();
                        try {
                            meditor.putString("id_user", response.getString("id_user"));
                            meditor.putString("email", valores.getString("email"));
                            meditor.putString("ci", valores.getString("ci"));
                            meditor.putString("nombre", valores.getString("nombre"));
                            meditor.putString("apellido", valores.getString("apellido"));
                            meditor.putString("direccion", valores.getString("direccion"));
                            meditor.putString("zona", valores.getString("zona"));
                            meditor.putString("telefono", valores.getString("telefono"));
                            meditor.putString("id_vehiculo", valores.getString("id_vehiculo"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        meditor.apply();

                        Intent intent = new Intent(Join_tres.this, Home_main.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();//TODO: Saltar a la clase help

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Progress.showProgressLayout(false, mJoinFormView, mProgressView);
                        Barrasnack.show("Intentalo de nuevo",findViewById(android.R.id.content));

                        Log.i(TAG, "Error="+error);

                    }
                });


        VSingleton.getInstance(this).addToRequestQueue(jsObjRequest);

    }

}
