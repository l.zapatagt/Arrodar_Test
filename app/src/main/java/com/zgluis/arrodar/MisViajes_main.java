package com.zgluis.arrodar;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.zgluis.arrodar.adapter.Misviajes_adapter;
import com.zgluis.arrodar.app.AppController;
import com.zgluis.arrodar.model.Url;
import com.zgluis.arrodar.model.Viaje;
import com.zgluis.arrodar.util.Barrasnack;
import com.zgluis.arrodar.util.Progress;
import com.zgluis.arrodar.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MisViajes_main extends AppCompatActivity {
    private Misviajes_adapter adapter;
    private List<Viaje> viajeList = new ArrayList<>();
    SharedPreferences session;
    private static final String TAG = "Arrodar_MisViajes";
    private ProgressDialog pDialog;
    private TextView tv_empty;
    private String id_viaje,id_pasajero,id_chofer;
    AlertDialog.Builder builder;
    AlertDialog alertCV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.misviajes_main);

        session = getSharedPreferences("session", Context.MODE_PRIVATE);

        //elementos de la vista
        tv_empty = (TextView) findViewById(R.id.tv_empty);

        //Inicializando vibrador

        //Creando Alert y perzonalidandolo
        builder = new AlertDialog.Builder(this);
        builder.setTitle("Calificar Viaje");
        builder.setMessage("Seleccione la calificacion que le da al viaje");
        builder.setPositiveButton("Positivo", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                Progress.showProgressDialog(true,pDialog,"Enviando");
                actualizarCalificacion(id_viaje, id_chofer, id_pasajero, getApplication().getString(R.string.cal_positivo));
                dialog.dismiss();
            }
        });
        builder.setNeutralButton("Neutral", new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Progress.showProgressDialog(true,pDialog,"Enviando");
                actualizarCalificacion(id_viaje,id_chofer,id_pasajero,getApplication().getString(R.string.cal_neutral));

                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Negativo", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Progress.showProgressDialog(true,pDialog,"Enviando");
                actualizarCalificacion(id_viaje,id_chofer,id_pasajero,getApplication().getString(R.string.cal_negativo));
                dialog.dismiss();
            }
        });
        alertCV = builder.create();

        //Listview con el contenido
        ListView listView;
        listView = (ListView) findViewById(R.id.listMisViajes);
        adapter = new Misviajes_adapter(this, viajeList);
        listView.setAdapter(adapter);//
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Viaje viaje = (Viaje) parent.getAdapter().getItem   (position);
                id_viaje = viaje.getId_viaje();
                id_pasajero = session.getString("id_user", "");
                id_chofer = viaje.getId_chofer();

                Log.i(TAG,id_viaje+"|"+id_pasajero+"|"+id_chofer);
                if(id_pasajero.equals(id_chofer)){
                    Utils.vibrarWarning(getApplicationContext());
                    Barrasnack.show("No puedes calificar tu propio viaje",findViewById(android.R.id.content));
                }
                else
                {
                alertCV.show();
                }
            }
        });

        pDialog = new ProgressDialog(this);
        Progress.showProgressDialog(true, pDialog, "Cargando...");

        consultarServicio();
    }

    private void consultarServicio() {
        viajeList.clear();
        adapter.notifyDataSetChanged();
        Log.i(TAG,Url.Obtener_ViajesByUser+"?id_user="+session.getString("id_user", ""));
        JsonArrayRequest viajeReq = new JsonArrayRequest(
                Url.Obtener_ViajesByUser+"?id_user="+session.getString("id_user", ""),
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        Progress.showProgressDialog(false, pDialog, "");
                        int x = response.length();
                        // Parsing json
                        for (int i = 0; i < x; i++) {
                            try {

                                JSONObject obj = response.getJSONObject(i);
                                Viaje viaje = new Viaje();

                                viaje.setId_viaje(obj.getString("id_viaje"));
                                viaje.setId_chofer(obj.getString("id_chofer"));
                                viaje.setNombre(obj.getString("nombre"));
                                viaje.setApellido(obj.getString("apellido"));
                                viaje.setReputacion(obj.getString("reputacion"));
                                viaje.setThumbnailUrl(Url.index + "images/" + obj.getString("img_url"));
                                viaje.setSalida(obj.getString("id_salida"));
                                viaje.setLlegada(obj.getString("id_llegada"));
                                viaje.setFecha(obj.getString("fecha"));
                                viaje.setHora(obj.getString("hora"));
                                viaje.setEstatus(obj.getString("estatus"));
                                viaje.setTelefono(obj.getString("telefono"));
                                viaje.setCorreo(obj.getString("email"));
                                viaje.setMicalificacion(obj.getString("micalificacion"));

                                //pasajeros
                                JSONArray psj = obj.getJSONArray("pasajeros");
                                int z = psj.length();
                                String aux2 = "";
                                System.out.println("Catnidad de pasajeros:" + z );
                                for(int y=0; y < z;y++){
                                    JSONObject pasajero = psj.getJSONObject(y);
                                    String aux1 = pasajero.getString("nombre") + " " +pasajero.getString("apellido") + ", ";
                                    System.out.println("Pasajero numero:" + y + aux1);
                                    aux2 = aux2+aux1;
                                }
                                System.out.println("ListaPasajeros:" + aux2 );

                                viaje.setMarca(obj.getString("marca"));
                                viaje.setModelo(obj.getString("modelo"));
                                viaje.setPlaca(obj.getString("placa"));
                                viaje.setPasajeros(aux2);
                                Log.i(TAG, Url.index + "images/" + obj.getString("img_url"));
                                // adding viaje to movies array
                                viajeList.add(viaje);

                            } catch (JSONException e) {
                                Barrasnack.show("Ocurrio un error consultando el servicio",findViewById(android.R.id.content));
                                e.printStackTrace();
                            }
                        }
                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());

                Progress.showProgressDialog(false, pDialog, "");
                tv_empty.setText(getApplication().getString(R.string.empty));

            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(viajeReq);

    }


    private void actualizarCalificacion(final String id_viaje,
                                        final String id_chofer,
                                        final String id_pasajero,
                                        final String calificacion) {
        Log.i(TAG, Url.Actualizar_Calificacion);
        JSONObject obj = new JSONObject();
        try {
            obj.put("id_viaje", id_viaje);
            obj.put("id_chofer",id_chofer);
            obj.put("id_pasajero", id_pasajero);
            obj.put("calificacion", calificacion);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(TAG+" valores:"+obj);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, Url.Actualizar_Calificacion, obj, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Progress.showProgressDialog(false, pDialog, "");
                        Log.i(TAG, "Respuesta=" + response);
                        Barrasnack.show("Viaje calificado", findViewById(android.R.id.content));
                        consultarServicio();

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Progress.showProgressDialog(false, pDialog, "");
                        Barrasnack.show("Intenta de nuevo", findViewById(android.R.id.content));
                        Log.i(TAG, "El error:" + error);
                    }
                });

        VSingleton.getInstance(this).addToRequestQueue(jsObjRequest);
    }


    public void btnConsultar(View v){
        Progress.showProgressDialog(true, pDialog, "Cargando");
        tv_empty.setText("");
        consultarServicio();
    }

}
//http://android.leocardz.com/animated-expanding-listview/