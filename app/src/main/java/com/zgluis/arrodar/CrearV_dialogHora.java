package com.zgluis.arrodar;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

@SuppressLint("ValidFragment")
public class CrearV_dialogHora extends DialogFragment implements TimePickerDialog.OnTimeSetListener{

    EditText et_hora;
    CrearV_dialogHora(EditText tv){
        et_hora = tv;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState){

        final Calendar c= Calendar.getInstance();
        int hour = c.get(Calendar.HOUR);
        int minute = c.get(Calendar.MINUTE);
        return new TimePickerDialog(getActivity(),this,hour,minute,false);
    }

    @Override
    public void onTimeSet(TimePicker view, int h, int m) {
        //String hour = h + ":"+(m);
        et_hora.setText(getString(R.string.hora,h,m));

    }
}
