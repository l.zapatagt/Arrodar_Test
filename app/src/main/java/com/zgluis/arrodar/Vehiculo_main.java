package com.zgluis.arrodar;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.zgluis.arrodar.model.Url;
import com.zgluis.arrodar.util.Barrasnack;
import com.zgluis.arrodar.util.Progress;
import com.zgluis.arrodar.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;


public class Vehiculo_main extends AppCompatActivity{
    EditText et_marca,et_modelo,et_placa,et_año;
    String TAG = Utils.getTag(this);
    SharedPreferences session;
    SharedPreferences.Editor meditor;
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vehiculo_main);
        et_marca = (EditText)findViewById(R.id.et_marca);
        et_modelo = (EditText)findViewById(R.id.et_modelo);
        et_placa = (EditText)findViewById(R.id.et_placa);
        et_año = (EditText)findViewById(R.id.et_año);
        pDialog = new ProgressDialog(this);


        session = getSharedPreferences("session", Context.MODE_PRIVATE);

    }

    @Override
    protected void onStart() {
        super.onStart();
        fillForm();
    }

    private void fillForm() {
        Log.i(TAG,"fillform");
        et_marca.setText(session.getString("marca", ""));
        et_modelo.setText(session.getString("modelo", ""));
        et_placa.setText(session.getString("placa", ""));
        et_año.setText(session.getString("year", ""));
    }

    public void prepararVehiculo(View v){

        if(TextUtils.isEmpty(Utils.getValor(et_marca))||
                TextUtils.isEmpty(Utils.getValor(et_modelo))||
                TextUtils.isEmpty(Utils.getValor(et_placa))||
                TextUtils.isEmpty(Utils.getValor(et_año))){
            Barrasnack.show("Faltan Campos",findViewById(android.R.id.content));
        }
        else{


            JSONObject valores = new JSONObject();
            try {
                SharedPreferences session = getSharedPreferences("session", Context.MODE_PRIVATE);
                valores.put("id_user",session.getString("id_user", ""));
                valores.put("marca",Utils.getValor(et_marca));
                valores.put("modelo", Utils.getValor(et_modelo));
                valores.put("placa", Utils.getValor(et_placa));
                valores.put("year", Utils.getValor(et_año));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Progress.showProgressDialog(true, pDialog, "Enviando...");
            insertarVehiculo(valores);
        }
    }



    private void insertarVehiculo(final JSONObject valores) {
        Log.i(TAG, Url.Insertar_Vehiculo);
        System.out.println(valores);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, Url.Insertar_Vehiculo, valores, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Progress.showProgressDialog(false, pDialog, "");
                        Log.i(TAG, "Respuesta=" + response);

                        Log.d(TAG, "Exito en registro");
                        meditor = session.edit();
                        try {
                            meditor.putString("id_vehiculo", response.getString("id_vehiculo"));
                            meditor.putString("marca", valores.getString("marca"));
                            meditor.putString("modelo", valores.getString("modelo"));
                            meditor.putString("placa", valores.getString("placa"));
                            meditor.putString("year", valores.getString("year"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        meditor.apply();

                        Intent intent = new Intent(Vehiculo_main.this, Home_main.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Progress.showProgressDialog(false, pDialog, "");
                        Barrasnack.show("Intentalo de nuevo",findViewById(android.R.id.content));

                        Log.i(TAG, "Error="+error);

                    }
                });


        VSingleton.getInstance(this).addToRequestQueue(jsObjRequest);

    }

    }

