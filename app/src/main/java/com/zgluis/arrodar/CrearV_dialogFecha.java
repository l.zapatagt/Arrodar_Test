package com.zgluis.arrodar;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;
import java.util.Date;

@SuppressLint("ValidFragment")
public class CrearV_dialogFecha extends DialogFragment implements DatePickerDialog.OnDateSetListener{

    EditText et_fecha;

    public CrearV_dialogFecha(EditText et){
        et_fecha = et;
    }



    public Dialog onCreateDialog(Bundle savedInstanceState){

        final Calendar c= Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        c.add(Calendar.DATE, 31);
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
        dialog.getDatePicker().setMinDate(new Date().getTime());
        dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        return dialog;
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        et_fecha.setText(getString(R.string.fecha, year,month+1,day));
    }
}
