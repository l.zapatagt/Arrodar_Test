package com.zgluis.arrodar;

import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.zgluis.arrodar.model.Url;
import com.zgluis.arrodar.util.Barrasnack;
import com.zgluis.arrodar.util.Connections;
import com.zgluis.arrodar.util.Progress;
import com.zgluis.arrodar.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class CrearV_main extends AppCompatActivity {

    private EditText et_fecha,et_hora;
    private Spinner sp_salida,sp_llegada;
    private ArrayList<String> zonas= new ArrayList<>();
    private ArrayAdapter<String> adaptador;
    String zonaS,zonaL;
    private String TAG = Utils.getTag(this);
    private ProgressDialog pDialog;
    private AlertDialog.Builder builder;
    private AlertDialog alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reservarv_main);
        et_fecha = (EditText) findViewById(R.id.et_fecha);
        et_hora = (EditText) findViewById(R.id.et_hora);

        et_fecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CrearV_dialogFecha dialog = new CrearV_dialogFecha(et_fecha);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                dialog.show(ft, "Date picker");
            }
        });

        et_hora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CrearV_dialogHora dialog = new CrearV_dialogHora(et_hora);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                dialog.show(ft, "Date picker");
            }
        });
        pDialog = new ProgressDialog(this);
        Progress.showProgressDialog(true, pDialog, "Cargando...");

        sp_salida = (Spinner) findViewById(R.id.sp_salida);
        sp_llegada = (Spinner) findViewById(R.id.sp_llegada);

        adaptador = new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,zonas);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_salida.setAdapter(adaptador);
        sp_llegada.setAdapter(adaptador);


        sp_salida.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                zonaS = zonas.get(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        sp_llegada.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                zonaL = zonas.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        //Creando Alert y perzonalidandolo
        builder = new AlertDialog.Builder(this);
        builder.setMessage("Viaje creado");
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), Home_main.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        alert = builder.create();


        Connections.consultarZonas(zonas, adaptador, TAG, pDialog);


    }

    public void prepararViaje(View v){

        String salida = Utils.getValorSpinner(sp_salida);
        String llegada = Utils.getValorSpinner(sp_llegada);
        if(TextUtils.isEmpty(Utils.getValor(et_fecha))||TextUtils.isEmpty(Utils.getValor(et_hora))){
            Barrasnack.show("Faltan campos", findViewById(android.R.id.content));
        }
        else if(!salida.equals("UFT")&&!llegada.equals("UFT")||salida.equals(llegada)){
            Log.i(TAG, "Ninguna de la locaciones es la UFT o son iguales");

            Barrasnack.show("Locaciones incorrectas", findViewById(android.R.id.content));
        }else{
            Progress.showProgressDialog(true, pDialog, "Cargando...");

            Log.i(TAG,"locaciones ok");
            JSONObject valores = new JSONObject();
            try {
                SharedPreferences session = getSharedPreferences("session", Context.MODE_PRIVATE);
                valores.put("id_chofer",session.getString("id_user", ""));
                valores.put("id_vehiculo","1");
                valores.put("id_salida", salida);
                valores.put("id_llegada", llegada);
                valores.put("fecha", Utils.getValor(et_fecha));
                valores.put("hora", Utils.getValor(et_hora));
                valores.put("calificacion", "");
                valores.put("estatus", "");
                insertarViaje(valores);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    public void insertarViaje(final JSONObject valores) {
        Log.i(TAG, Url.Insertar_Viaje);
        System.out.println(valores);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, Url.Insertar_Viaje, valores, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Progress.showProgressDialog(false, pDialog, "");
                        Log.i(TAG, "Respuesta=" + response);
                        alert.show();

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Progress.showProgressDialog(false, pDialog,"");
                        Barrasnack.show("Intentalo de nuevo", findViewById(android.R.id.content));
                        Log.i(TAG, "Error=" + error);

                    }
                });


        VSingleton.getInstance(this).addToRequestQueue(jsObjRequest);

    }

}//fin class







