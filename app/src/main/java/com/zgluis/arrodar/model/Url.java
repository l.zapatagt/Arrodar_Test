package com.zgluis.arrodar.model;


public class Url {

    public static String index = "http://arrodar.esy.es/";
    public static String Reservar_Viaje = index + "reservar_viaje.php";
    public static String Actualizar_User = index + "actualizar_user.php";
    public static String Actualizar_Calificacion = index + "actualizar_calificacion.php";
    public static String Obtener_Viajes = index + "obtener_viajes.php";
    public static String Obtener_ViajesByUser = index + "obtener_viajes_por_user.php";
    public static String Login = index + "login_user.php";
    public static String Subir_Img = index + "subir_img.php";
    public static String Insertar_User = index + "insertar_user.php";
    public static String Insertar_Viaje = index + "insertar_viaje.php";
    public static String Insertar_Vehiculo = index + "insertar_vehiculo.php";
    public static String Obtener_Zonas = index + "obtener_zonas.php";
    public static String estatusImgUser=index +"actualizar_img_user.php";
}
