package com.zgluis.arrodar.model;


public class Viaje {
    private String id_viaje, id_chofer, nombre, apellido, telefono, correo,
            salida, llegada, fecha, hora, reputacion, estatus, thumbnailUrl,
            id_vehiculo, marca, modelo, placa, year, micalificacion,
            pasajeros;

    public Viaje(String id_viaje, String id_chofer, String name, String lastname,String telefono,String correo, String thumbnailUrl,
                 String salida, String llegada, String fecha, String hora, String reputacion, String estatus,
                 String id_vehiculo, String marca, String modelo, String placa, String year, String micalificacion,
                 String pasajeros)
    {
        this.id_chofer = id_chofer;
        this.id_viaje = id_viaje;
        this.nombre = name;
        this.apellido = lastname;
        this.telefono = telefono;
        this.correo = correo;
        this.thumbnailUrl = thumbnailUrl;
        this.salida = salida;
        this.llegada = llegada;
        this.fecha = fecha;
        this.hora = hora;
        this.reputacion = reputacion;
        this.estatus = estatus;
        this.micalificacion = micalificacion;

        this.pasajeros = pasajeros;

        this.id_vehiculo = id_vehiculo;
        this.marca = marca;
        this.modelo = modelo;
        this.placa = placa;
        this.year = year;


    }


    public Viaje() {
    }

    public String getId_viaje() {
        return id_viaje;
    }

    public void setId_viaje(String id_viaje) {
        this.id_viaje = id_viaje;
    }

    public String getId_chofer() {
        return id_chofer;
    }

    public void setId_chofer(String id_chofer) {
        this.id_chofer = id_chofer;
    }

    public String getMicalificacion() {
        return micalificacion;
    }

    public void setMicalificacion(String micalificacion) {
        this.micalificacion = micalificacion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getSalida() {
        return salida;
    }

    public void setSalida(String salida) {
        this.salida = salida;
    }

    public String getLlegada() {
        return llegada;
    }

    public void setLlegada(String llegada) {
        this.llegada = llegada;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getReputacion() {
        return reputacion;
    }

    public void setReputacion(String reputacion) {
        this.reputacion = reputacion;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public void setPasajeros(String pasajeros) {
        this.pasajeros = pasajeros;
    }

    public String getPasajeros() {
        return pasajeros;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public String getPlaca() {
        return placa;
    }
}